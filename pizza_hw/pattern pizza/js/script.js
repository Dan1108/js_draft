const price = document.querySelector('#priceTotal');
let change = 0;
const f = document.forms[0],
    allOptions = {
        small : 50,
        medium : 75,
        large : 100,
        sauces : {
            ketchup : 20,
            barbecue : 30,
            ricotta : 30
        },
        toppings : {
            regCheese : 13,
            fetaCheese : 17,
            mocarella : 19,
            ham : 15,
            tomatos : 10,
            mushrooms : 11
        }
    };
    // Следующие три блока для кнопок выбора маленькой, средней или большой, при клике на каждую кнопку цена увеличивается
const small = document.querySelector('#small'), 
    mid = document.querySelector('#mid'),
    big = document.querySelector('#big');
const addSmall = () => {
    if (change !== allOptions.small && change !== allOptions.medium && change !== allOptions.big && change < allOptions.small || change == 0) {  
        change += allOptions.small;
        price.textContent = `${change}`; 
    } else { 
        change = 0;
        change += allOptions.small;
        price.textContent = `${change}`; 
    }
   
}

const addMid = () => {
    if (change !== allOptions.small && change !== allOptions.medium && change !== allOptions.big && change <allOptions.medium || change == 0) {  
    change += allOptions.medium;
    price.textContent = `${change}`;
    } else {
        change = 0;
        change += allOptions.medium;
        price.textContent = `${change}`;
    }
}

const addBig = () => {
    if (change !== allOptions.small && change !== allOptions.medium && change !== allOptions.big && change <allOptions.big || change == 0) {  
    change += allOptions.large;
    price.textContent = `${change}`;
    } else {
        change = 0;
        change += allOptions.large;
        price.textContent = `${change}`;
    }
}

small.addEventListener('click', addSmall);
mid.addEventListener('click', addMid);
big.addEventListener('click', addBig);
//Дальше достал все основные элементы для перетаскивания
const ketchup = document.querySelector('#sauceClassic'),
    bbq = document.querySelector('#sauceBBQ'),
    rikotta = document.querySelector('#sauceRikotta'),
    regCheese = document.querySelector('#moc1'),
    feta = document.querySelector('#moc2'),
    mocarella = document.querySelector('#moc3'),
    ham = document.querySelector('#telya'),
    tomatos = document.querySelector('#vetch1'),
    mushrooms = document.querySelector('#vetch2'),
    pizza_img = document.querySelector('#pizza_img');//это фотка коржа на который буду перетаскивать 

ketchup.addEventListener('dragstart', function (event) { // старт перетаскивания
    this.style.cssText = `width: 348px; height: 232px`; // подстроил размеры перетаскиваемого кетчупа под корж(на который буду перетаскивать)
    event.dataTransfer.effectAllowed = "move";
    event.dataTransfer.setData("Text", this.id); // получаем айди того что перетаскиваем 
})
ketchup.addEventListener('dragend', function (event) { // cбросил стили, когда отпустил кнопку мыши
    this.style.cssText = ``;
})
pizza_img.addEventListener('dragenter', function (event){ // подсветил края коржа , когда попали кетчупом в область коржа
    this.style.border = "3px #FFC0CB";
})
pizza_img.addEventListener('dragleave', function (event) { // убрали подсвет, если покинули область в которую перетаскиваем 
    this.style.border = "";
})
pizza_img.addEventListener('dragover', function (event) { // отменяем дефолтные дейсвия браузера , если они есть, чтобы не мешали нашему процессу перетаскивания
    if(event.preventDefault) event.preventDefault();
    return false; // зачем эта строчка , я не понял , но так было в твоем коде, решил оставить
})
pizza_img.addEventListener('drop', function (event) { // выполниться , когда отпускаем кнопку мыши
    if(event.preventDefault) event.preventDefault(); // снова отменяем дефолт
    if(event.stopPropagation) event.stopPropagation();

    this.style.border = ""; // убираем подсветку 

    const id = event.dataTransfer.getData("Text"); // получаем инфу о объекте которые перетащили

    const elem = document.querySelector(id); // зачем нужна эта строчка, и предыдущая тоже не понял 
    const div = document.createElement("div"); // создаем новый див
    div.classList.add("draggable", "ketchup"); // добавляем стили  width: 348px;  height: 232px; 
    //  background-image: url(Pizza_pictures/klassicheskij-bortik_1556622914630.png); z-index: 1;
    // 

    this.append(div); // добавляем новый элемент, который должен быть копией кетчупа, на корж
    
    return false; // зачем эта строчка, снова не понял
})






// \b[a-z0-9._]+@[a-z0-9.-]+\.[a-z]{2,4}\b
